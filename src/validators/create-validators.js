import { isObject } from "./is-object";

export function createValidators(model) {
  return Object.entries(model).reduce(
    (validationPredicates, [property, dataType]) => {
      switch (dataType) {
        case "string": {
          return {
            ...validationPredicates,
            [property]: (arg) => typeof arg === "string",
          };
        }
        case "number": {
          return {
            ...validationPredicates,
            [property]: (arg) => typeof arg === "number" && arg === arg,
          };
        }
        case "boolean": {
          return {
            ...validationPredicates,
            [property]: (arg) => typeof arg === "boolean",
          };
        }
        case "array": {
          return {
            ...validationPredicates,
            [property]: (arg) => Array.isArray(arg),
          };
        }
        case "object": {
          return {
            ...validationPredicates,
            [property]: (arg) => isObject(arg),
          };
        }
        default:
          return validationPredicates;
      }
    },
    {}
  );
}
