import { createValidators } from "./create-validators";
import { barModel } from "../model/bar";

describe("createValidators function", () => {
  it("should return an object of validators", () => {
    const barsValidator = createValidators(barModel);
    expect(barsValidator).toMatchSnapshot();
  });
});
