import { barModel } from "../model/bar";
import { carModel } from "../model/car";
import { personModel } from "../model/person";
import { isValidData } from "./evaluate-response-data";
import { barObj, barObjF } from "../model/bar";
import { carObj, carObjF } from "../model/car";
import { personObj, personObjF, personObjF2 } from "../model/person";

describe("when evaluating response data ", () => {
  describe("for car data", () => {
    it("should return true for correct data", () => {
      expect(isValidData(carModel, carObj)).toBe(true);
    });
    it("should return false for incorrect data", () => {
      expect(isValidData(carModel, carObjF)).toBe(false);
    });
  });
  describe("for bar data", () => {
    it("should return true for correct data", () => {
      expect(isValidData(barModel, barObj)).toBe(true);
    });
    it("should return false for incorrect data", () => {
      expect(isValidData(barModel, barObjF)).toBe(false);
    });
  });
  describe("for person data", () => {
    it("should return true for correct data", () => {
      expect(isValidData(personModel, personObj)).toBe(true);
    });
    it("should return false for incorrect data", () => {
      expect(isValidData(personModel, personObjF)).toBe(false);
    });
    it("should return false for incorrect data - 2", () => {
      expect(isValidData(personModel, personObjF2)).toBe(false);
    });
  });
});
