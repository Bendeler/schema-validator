import { createValidators } from "./create-validators";

export function createDataModelValidator(model) {
  const validators = createValidators(model);
  return (dataToBeValidated) => {
    return Object.entries(validators).reduce(
      (dataEvaluations, [property, dataTypeValidator]) => {
        const valueToBeTested = dataToBeValidated[property];
        const valueIsValid = !!dataTypeValidator(valueToBeTested);

        return {
          ...dataEvaluations,
          [property]: {
            isValid: valueIsValid,
          },
        };
      },
      {}
    );
  };
}
