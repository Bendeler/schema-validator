export function isObject(value) {
  if (!value) return false;
  if (Array.isArray(value)) return false;
  if (value instanceof Boolean) return false;
  if (value instanceof Number) return false;
  if (value instanceof String) return false;
  return value === Object(value);
}
