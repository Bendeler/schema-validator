import { barModel, barObj, barObjF } from "../model/bar";
import { carModel, carObj, carObjF } from "../model/car";
import {
  personModel,
  personObj,
  personObjF,
  personObjF2,
} from "../model/person";
import { createDataModelValidator } from "../validators/create-data-model-validator";

describe("createDataModelValidator", () => {
  describe("for bar Model", () => {
    const barDataValidator = createDataModelValidator(barModel);
    it("should return an object of results of which entry has a valid dataType", () => {
      const result = barDataValidator(barObj);
      expect(result).toMatchSnapshot();
    });
    it("should return an object of results of which drinks has an invalid dataType", () => {
      const result = barDataValidator(barObjF);
      expect(result).toMatchSnapshot();
      expect(result.drinks.isValid).toBe(false);
    });
  });
  describe("for car Model", () => {
    const carDataValidator = createDataModelValidator(carModel);
    it("should return an object of results of which entry has a valid dataType", () => {
      const result = carDataValidator(carObj);
      expect(result).toMatchSnapshot();
    });
    it("should return an object of results of which mileage has an invalid dataType", () => {
      const result = carDataValidator(carObjF);
      expect(result).toMatchSnapshot();
      expect(result.milage.isValid).toBe(false);
    });
  });
  describe("for person Model", () => {
    const personDataValidator = createDataModelValidator(personModel);
    it("should return an object of results of which entry has a valid dataType", () => {
      const result = personDataValidator(personObj);
      expect(result).toMatchSnapshot();
    });
    it("should return an object of results of which missing properties have an invalid dataType", () => {
      const result = personDataValidator(personObjF);
      expect(result).toMatchSnapshot();
      expect(result.siblings.isValid).toBe(false);
      expect(result.metaData.isValid).toBe(false);
    });
    it("should return an object of results of which two siblings and metaData have an invalid dataType", () => {
      const result = personDataValidator(personObjF2);
      expect(result).toMatchSnapshot();
      expect(result.siblings.isValid).toBe(false);
      expect(result.metaData.isValid).toBe(false);
    });
  });
});
