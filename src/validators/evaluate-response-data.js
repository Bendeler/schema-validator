import { createDataModelValidator } from "./create-data-model-validator";

export const isValidData = (model, data) =>
  Object.values(createDataModelValidator(model)(data)).every(
    ({ isValid }) => isValid
  );
