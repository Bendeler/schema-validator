import { isObject } from "./is-object";

describe("is-object validator", () => {
  describe("should return false when", () => {
    it("argument is a number", () => {
      expect(isObject(1)).toBe(false);
      expect(isObject(0)).toBe(false);
      expect(isObject(-1)).toBe(false);
    });
    it("argument is a string", () => {
      expect(isObject("")).toBe(false);
      expect(isObject("string")).toBe(false);
    });
    it("argument is a boolean", () => {
      expect(isObject(true)).toBe(false);
      expect(isObject(false)).toBe(false);
    });
    it("argument is from a boolean constructor", () => {
      expect(isObject(new Boolean(true))).toBe(false);
      expect(isObject(new Boolean(false))).toBe(false);
    });
    it("argument is an array", () => {
      expect(isObject([])).toBe(false);
    });
    it("argument is from an array constructor", () => {
      expect(isObject(new Array(1))).toBe(false);
    });
    it("argument is NaN", () => {
      expect(isObject(NaN)).toBe(false);
    });
    it("argument is null", () => {
      expect(isObject(null)).toBe(false);
    });
    it("argument is missing", () => {
      expect(isObject()).toBe(false);
    });
    it("argument is undefined", () => {
      expect(isObject(undefined)).toBe(false);
    });
  });
  describe("should return true when", () => {
    it("argument is an empty object literal", () => {
      expect(isObject({})).toBe(true);
    });
    it("argument is an object literal", () => {
      expect(isObject({ hello: "is it me you 're looking for?" })).toBe(true);
    });
    it("argument is from an object constructor", () => {
      expect(
        isObject(new Object({ hello: "is it me you 're looking for?" }))
      ).toBe(true);
    });
  });
});
