export const personModel = {
  name: "string",
  age: "number",
  siblings: "array",
  metaData: "object",
  active: "boolean",
};

// Validates true
export const personObj = {
  name: "James",
  age: 25,
  siblings: ["Johnnathan"],
  metaData: {},
  active: true,
};

// This is missing properties and assignments says that props will always be there!
export const personObjF = {
  name: "James",
  age: 25,
  active: true,
};

export const personObjF2 = {
  name: "James",
  age: 25,
  active: true,
  siblings: undefined,
  metaData: null,
};
